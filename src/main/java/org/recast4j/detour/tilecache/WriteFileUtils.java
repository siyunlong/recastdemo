package org.recast4j.detour.tilecache;

import cn.hutool.core.io.FileUtil;
import org.recast4j.detour.NavMeshQuery;
import org.recast4j.detour.io.MeshSetWriter;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteOrder;

/**
 * 写入
 *
 * @author 司云龙
 * @version 1.0
 * @date 2022/1/20 12:04
 */
public class WriteFileUtils {
    public static void readFile(NavMeshQuery m_navQuery, String path) {
        OutputStream outputStream = null;
        try {
            FileUtil.del(path);
            outputStream = FileUtil.getOutputStream(path);
            MeshSetWriter meshSetWriter = new MeshSetWriter();
            meshSetWriter.write(outputStream, m_navQuery.getAttachedNavMesh(), ByteOrder.BIG_ENDIAN, true);

        }catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                    outputStream.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


    }
}
