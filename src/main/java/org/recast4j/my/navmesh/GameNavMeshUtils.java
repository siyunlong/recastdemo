package org.recast4j.my.navmesh;


import cn.hutool.core.io.resource.ResourceUtil;
import org.recast4j.demo.builder.SoloNavMeshBuilder;
import org.recast4j.demo.geom.DemoInputGeomProvider;
import org.recast4j.demo.io.ObjImporter;
import org.recast4j.detour.MeshData;
import org.recast4j.detour.NavMesh;
import org.recast4j.detour.Tupple2;
import org.recast4j.recast.RecastBuilder;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

/**
 * 构造器
 *
 * @author 司云龙
 * @version 1.0
 * @date 2021/12/30 16:41
 */
public class GameNavMeshUtils {
    public static MeshData build(String filename)  {
        try {
            SoloNavMeshBuilder soloNavMeshBuilder = new SoloNavMeshBuilder();
            InputStream stream = ResourceUtil.getStream(filename);
            DemoInputGeomProvider geom = new ObjImporter().load(stream);

            MeshData meshData = soloNavMeshBuilder.build(geom);
            return meshData;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public static NavMeshBuilder buildForNavMesh(String filename)  {
        try {
            SoloNavMeshBuilder soloNavMeshBuilder = new SoloNavMeshBuilder();
            InputStream stream = ResourceUtil.getStream(filename);
            DemoInputGeomProvider geom = new ObjImporter().load(stream);


            Tupple2<List<RecastBuilder.RecastBuilderResult>, NavMesh> ressult = soloNavMeshBuilder.buildForNavMesh(geom);
            return new NavMeshBuilder(ressult.first, ressult.second);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {
        long now = System.currentTimeMillis();
        NavMeshBuilder builder = buildForNavMesh("F://game_gz.obj");

        System.out.println(System.currentTimeMillis()-now);
    }

}
