package org.recast4j.my.navmesh;

import org.recast4j.detour.NavMesh;
import org.recast4j.detour.NavMeshQuery;
import org.recast4j.recast.RecastBuilder;

import java.util.List;

/**
 * TODO
 *
 * @author 司云龙
 * @version 1.0
 * @date 2022/1/14 10:35
 */
public class NavMeshBuilder {
    private NavMesh navMesh;
    private NavMeshQuery navMeshQuery;
    private List<RecastBuilder.RecastBuilderResult> recastResults;


    public NavMeshBuilder(List<RecastBuilder.RecastBuilderResult> recastResults, NavMesh navMesh) {
        this.navMesh = navMesh;
        this.recastResults = recastResults;
        this.navMesh = navMesh;
        setQuery(navMesh);
    }

    private void setQuery(NavMesh navMesh) {
        navMeshQuery = navMesh != null ? new NavMeshQuery(navMesh) : null;
    }
}
