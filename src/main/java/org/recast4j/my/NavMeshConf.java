package org.recast4j.my;

import org.recast4j.recast.RecastConstants;

/**
 * 多边形生成方式
 *
 * @author 司云龙
 * @version 1.0
 * @date 2021/12/30 16:47
 */
public abstract class NavMeshConf {
    public static final RecastConstants.PartitionType m_partitionType = RecastConstants.PartitionType.WATERSHED;

    public static float m_cellSize = 0.3F;
    public static float m_cellHeight = 0.01F;
    public static float m_agentHeight = 0.1F;
    public static float m_agentRadius = 0F;
    public static float m_agentMaxClimb = 0.6F;
    public static float m_agentMaxSlope = 45.0F;
    //合并区域
    public static int m_regionMinSize = 8;
    public static int m_regionMergeSize = 20;

    //多边形的边最大长度
    public static float m_edgeMaxLen = 12;
    public static float m_edgeMaxError = 1.3f;

    //合并三角形是可以合并的最大顶点数
    public static int MaxPoly = 6;
    public static int m_vertsPerPoly = MaxPoly;

    public static float m_detailSampleDist = 6;
    public static float m_detailSampleMaxError = 1;


    public static final boolean filterLowHangingObstacles = true;
    public static final boolean filterLedgeSpans = true;
    public static final boolean filterWalkableLowHeightSpans = true;


}
