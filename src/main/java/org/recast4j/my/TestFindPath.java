package org.recast4j.my;


import cn.hutool.core.io.FileUtil;
import org.recast4j.detour.MeshData;
import org.recast4j.detour.NavMesh;
import org.recast4j.detour.io.MeshSetWriter;
import org.recast4j.my.navmesh.GameNavMeshUtils;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.List;

/**
 * 寻路测试
 *
 * @author 司云龙
 * @version 1.0
 * @date 2021/12/21 14:23
 */
public class TestFindPath {
    public static void main(String[] args) throws IOException {
        testFindByObjFile("F://1000.obj");
    }

    public static void testFindByObjFile(String fileName) {
        testFind(GameNavMeshUtils.build(fileName));
    }


    public static void testFind(MeshData meshData) {
        Easy3dNav nav = new Easy3dNav(true,true);
        nav.init(meshData);

        float[] startPos = new float[]{465.62207f, 0.0f, 671.2439f};
        float[] endPos = new float[]{896.43225f, 0.0f, 343.65076f};

        List<float[]> pathList = nav.find(startPos, endPos);
        for (float[] floats : pathList) {
            System.out.println(Arrays.toString(floats));
        }
        System.out.println("pathList size:"+pathList.size());
    }


}
